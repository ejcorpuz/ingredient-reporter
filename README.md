# Ingredient Reporter

An API that allows you to manage your recipes and orders, and can also give you a basic calculated report of the ingredients you needed to buy.

# Requirements
 - Docker
 - Docker Compose
 
# Setup
```
git clone git@gitlab.com:ejcorpuz/ingredient-reporter.git
cd ingredient-reporter
chmod +x ./start.sh
./start.sh
```

# API Documentation

* [Ingredients](#ingredients)
  * [Create Ingredient](#1-create-ingredient)
  * [Get Ingredients](#2-get-ingredients)
  * [Delete Ingredient](#3-delete-ingredient)


* [Recipes](#recipes)
  * [Create Recipe](#1-create-recipe)
  * [Get Recipes](#2-get-recipes)
  * [Delete Recipe](#3-delete-recipe)


* [Boxes](#boxes)
  * [Create Box](#1-create-box)
  * [Get Boxes](#2-get-boxes)
  * [Delete Box](#3-delete-box)


* [Supply](#supply)
  * [Compute Supply](#1-compute-supply)


--------


## Ingredients



### 1. Create Ingredient



***Endpoint:***

```bash
Method: POST
Type: FORMDATA
URL: localhost:8080/ingredients/create
```



***Body:***

| Key | Value | Description |
| --- | ------|-------------|
| name | pepper | Required |
| measure | kg | Required |
| supplier | sunflower | Required |



### 2. Get Ingredients



***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:8080/ingredients
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| page | 1 | Optional |
| limit | 10 | Optional |
| supplier |  | Optional |



### 3. Delete Ingredient



***Endpoint:***

```bash
Method: DELETE
Type: 
URL: localhost:8080/ingredients/delete
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| id | 2 | Required |



## Recipes



### 1. Create Recipe



***Endpoint:***

```bash
Method: POST
Type: FORMDATA
URL: localhost:8080/recipes/create
```



***Body:***

| Key | Value | Description |
| --- | ------|-------------|
| name | Bulalo | Required |
| description | The Philippines Best | Optional |
| ingredients[0][ingredient_id] | 1 | Required |
| ingredients[0][amount] | 30 | Required |



### 2. Get Recipes



***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:8080/recipes
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| page | 1 | Optional |
| limit | 10 | Optional |



### 3. Delete Recipe



***Endpoint:***

```bash
Method: DELETE
Type: 
URL: localhost:8080/recipes/delete
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| id | 1 | Required |



## Boxes



### 1. Create Box



***Endpoint:***

```bash
Method: POST
Type: FORMDATA
URL: localhost:8080/boxes/create
```



***Body:***

| Key | Value | Description |
| --- | ------|-------------|
| delivery_date | 2020-04-01 | Required |
| recipes[0] | 5 | Optional |



### 2. Get Boxes



***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:8080/boxes
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| page | 1 | Optional |
| limit | 10 | Optional |
| delivery_date |  | Optional |



### 3. Delete Box



***Endpoint:***

```bash
Method: DELETE
Type: 
URL: localhost:8080/boxes/delete
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| id | 1 | Required |



## Supply



### 1. Compute Supply



***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:8080/supply/compute
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| order_date | 2020-03-31 | Required |



---
### Author
[Ej Corpuz](https://ejcorpuz.me)