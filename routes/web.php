<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**
 * @var \Laravel\Lumen\Routing\Router $router
 */
$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => '/ingredients', 'namespace' => '\App\Http\Controllers'], function () use ($router) {
    $router->get('/', 'IngredientController@all');
    $router->post('/create', 'IngredientController@create');
    $router->delete('/delete', 'IngredientController@delete');
});

$router->group(['prefix' => '/recipes', 'namespace' => '\App\Http\Controllers'], function () use ($router) {
    $router->get('/', 'RecipeController@all');
    $router->post('/create', 'RecipeController@create');
    $router->delete('/delete', 'RecipeController@delete');
});

$router->group(['prefix' => '/boxes', 'namespace' => '\App\Http\Controllers'], function () use ($router) {
    $router->get('/', 'UserBoxController@all');
    $router->post('/create', 'UserBoxController@create');
    $router->delete('/delete', 'UserBoxController@delete');
});

$router->group(['prefix' => '/supply', 'namespace' => '\App\Http\Controllers'], function () use ($router) {
    $router->get('/compute', 'SupplyController@compute');
});