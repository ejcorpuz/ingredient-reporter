#!/usr/bin/env bash

set -e

cat .env.example > .env
echo Starting services
docker-compose up -d
until docker-compose exec mysql mysql -h 127.0.0.1 -u root -ppassword --silent -e "show databases;"
do
  echo "Waiting for database connection..."
  sleep 5
done
echo Installing dependencies
docker pull composer
docker run --rm --volume ${PWD}/:/app composer install --ignore-platform-reqs
echo Seeding database
docker-compose run --rm cli php artisan migrate:fresh --force && echo Database migrated
docker-compose run --rm cli php artisan db:seed --force && echo Database seeded
docker-compose run --rm cli php vendor/bin/phpunit --configuration phpunit.xml --testdox