<?php

/**
 * Class RecipeControllerTest
 */
class RecipeControllerTest extends TestCase
{
    public function testCreateWithValidParameters()
    {
        $this->post('/recipes/create', [
            'name' => 'Test Recipe Name',
            'description' => 'Test Description',
            'ingredients' => [
                ['ingredient_id' => 2, 'amount' => 5],
                ['ingredient_id' => 3, 'amount' => 10],
                ['ingredient_id' => 4, 'amount' => 15],
            ],
        ]);
        $this->assertResponseOk();
    }

    public function testCreateWithoutNameShouldThrowAnError()
    {
        $this->post('/recipes/create', [
            'description' => 'Test Description',
            'ingredients' => [
                ['ingredient_id' => 2, 'amount' => 5],
                ['ingredient_id' => 3, 'amount' => 10],
                ['ingredient_id' => 4, 'amount' => 15],
            ],
        ]);
        $this->seeJsonContains([
            'message' => 'Recipe name is required.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testCreateWithoutNonStringNameShouldThrowAnError()
    {
        $this->post('/recipes/create', [
            'name' => 1,
            'description' => 'Test Description',
            'ingredients' => [
                ['ingredient_id' => 2, 'amount' => 5],
                ['ingredient_id' => 3, 'amount' => 10],
                ['ingredient_id' => 4, 'amount' => 15],
            ],
        ]);
        $this->seeJsonContains([
            'message' => 'Recipe name should be a valid string.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testCreateWithNonUniqueNameShouldThrowAnError()
    {
        $this->post('/recipes/create', [
            'name' => 'Test Recipe Name',
            'description' => 'Test Description',
            'ingredients' => [
                ['ingredient_id' => 2, 'amount' => 5],
                ['ingredient_id' => 3, 'amount' => 10],
                ['ingredient_id' => 4, 'amount' => 15],
            ],
        ]);
        $this->seeJsonContains([
            'message' => 'Recipe with the same name already exist.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testCreateWithoutIngredientsShouldThrowAnError()
    {
        $this->post('/recipes/create', [
            'name' => 'Test Recipe Name Two',
            'description' => 'Test Description',
        ]);
        $this->seeJsonContains([
            'message' => 'Ingredients is required to create a recipe.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testCreateWithInvalidIngredientCollectionShouldThrowAnError()
    {
        $this->post('/recipes/create', [
            'name' => 'Test Recipe Name Two',
            'description' => 'Test Description',
            'ingredients' => true
        ]);
        $this->seeJsonContains([
            'message' => 'Ingredients must be a list or collection.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testCreateWithoutIngredientIdShouldThrowAnError()
    {
        $this->post('/recipes/create', [
            'name' => 'Test Recipe Name Two',
            'description' => 'Test Description',
            'ingredients' => [
                ['amount' => 5],
            ]
        ]);
        $this->seeJsonContains([
            'message' => 'A valid ingredient is required.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testCreateWithInvalidIngredientIdShouldThrowAnError()
    {
        $this->post('/recipes/create', [
            'name' => 'Test Recipe Name Two',
            'description' => 'Test Description',
            'ingredients' => [
                ['ingredient_id' => 1000, 'amount' => 5],
            ]
        ]);
        $this->seeJsonContains([
            'message' => 'A valid ingredient is required.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testCreateWithoutAmountShouldThrowAnError()
    {
        $this->post('/recipes/create', [
            'name' => 'Test Recipe Name Two',
            'description' => 'Test Description',
            'ingredients' => [
                ['ingredient_id' => 2],
            ]
        ]);
        $this->seeJsonContains([
            'message' => 'Ingredient amount is required.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testCreateWithInvalidAmountShouldThrowAnError()
    {
        $this->post('/recipes/create', [
            'name' => 'Test Recipe Name Two',
            'description' => 'Test Description',
            'ingredients' => [
                ['ingredient_id' => 2, 'amount' => true],
            ]
        ]);
        $this->seeJsonContains([
            'message' => 'Ingredient amount should be a valid value.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testAllWillReturnPaginatedItems()
    {
        $this->get('/recipes');
        $this->seeJsonStructure([
            'result',
            'page',
            'limit',
            'pages',
            'total'
        ]);
        $this->assertResponseOk();
    }

    public function testAllLimitToFiveItems()
    {
        $response = $this->get('/recipes?limit=5');
        $content = json_decode($response->response->content());
        $this->seeJsonStructure([
            'result',
            'page',
            'limit',
            'pages',
            'total'
        ]);
        $this->assertCount(5, $content->result);
        $this->assertResponseOk();
    }

    public function testDeleteWithValidId()
    {
        $this->delete('/recipes/delete', ['id' => 1]);
        $this->seeJsonContains(['result' => true]);
        $this->assertResponseOk();
    }

    public function testDeleteWithNonExistingIdShouldReturnFalse()
    {
        $this->delete('/recipes/delete', ['id' => 1000]);
        $this->seeJsonContains(['result' => false]);
        $this->assertResponseOk();
    }

    public function testDeleteWithoutIdShouldThrowAnError()
    {
        $this->delete('/recipes/delete');
        $this->seeJsonContains([
            'message' => 'Recipe id is required.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testDeleteWithNonIntegerIdShouldThrowAnError()
    {
        $this->delete('/recipes/delete', ['id' => 'abc']);
        $this->seeJsonContains([
            'message' => 'Recipe id must be an integer.'
        ]);
        $this->assertResponseStatus(500);
    }
}
