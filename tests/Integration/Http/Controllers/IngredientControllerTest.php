<?php

/**
 * Class IngredientControllerTest
 */
class IngredientControllerTest extends TestCase
{
    public function testCreateWithValidParameters()
    {
        $this->post('/ingredients/create', [
            'name' => 'Test Ingredient',
            'measure' => 'g',
            'supplier' => 'Test Supplier'
        ]);
        $this->seeJsonContains([
            'name' => 'Test Ingredient',
            'measure' => 'g',
            'supplier' => 'Test Supplier'
        ]);
        $this->assertResponseOk();
    }

    public function testCreateWithoutNameShouldThrowAnErrorMessage()
    {
        $this->post('/ingredients/create', [
            'measure' => 'g',
            'supplier' => 'Test Supplier'
        ]);
        $this->seeJsonContains([
            'message' => 'Ingredient name is required.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testCreateWithNonStringNameShouldThrowAnErrorMessage()
    {
        $this->post('/ingredients/create', [
            'name' => 1,
            'measure' => 'g',
            'supplier' => 'Test Supplier'
        ]);
        $this->seeJsonContains([
            'message' => 'Ingredient name should be a valid string.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testCreateWithNonUniqueNameShouldThrowAnErrorMessage()
    {
        $this->post('/ingredients/create', [
            'name' => 'Test Ingredient',
            'measure' => 'g',
            'supplier' => 'Test Supplier'
        ]);
        $this->seeJsonContains([
            'message' => 'Ingredient with the same name already exist.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testCreateWithoutMeasureShouldThrowAnErrorMessage()
    {
        $this->post('/ingredients/create', [
            'name' => 'Test Ingredient Two',
            'supplier' => 'Test Supplier'
        ]);
        $this->seeJsonContains([
            'message' => 'Ingredient measure is required, either g, kg, pieces.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testCreateWithInvalidMessageShouldThrowAnErrorMessage()
    {
        $this->post('/ingredients/create', [
            'name' => 'Test Ingredient Two',
            'measure' => 'meter',
            'supplier' => 'Test Supplier'
        ]);
        $this->seeJsonContains([
            'message' => 'Ingredient measure should be either g, kg, pieces.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testCreateWithoutSupplierShouldThrowAnErrorMessage()
    {
        $this->post('/ingredients/create', [
            'name' => 'Test Ingredient Three',
            'measure' => 'kg',
        ]);
        $this->seeJsonContains([
            'message' => 'Ingredient supplier name is required.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testCreateWithNonStringSupplierShouldThrowAnErrorMessage()
    {
        $this->post('/ingredients/create', [
            'name' => 'Test Ingredient Three',
            'measure' => 'kg',
            'supplier' => 1,
        ]);
        $this->seeJsonContains([
            'message' => 'Ingredient supplier name should be a valid string.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testAllWillReturnPaginatedItems()
    {
        $this->get('/ingredients');
        $this->seeJsonStructure([
            'result',
            'page',
            'limit',
            'pages',
            'total'
        ]);
        $this->assertResponseOk();
    }

    public function testAllLimitToFiveItems()
    {
        $response = $this->get('/ingredients?limit=5');
        $content = json_decode($response->response->content());
        $this->seeJsonStructure([
            'result',
            'page',
            'limit',
            'pages',
            'total'
        ]);
        $this->assertCount(5, $content->result);
        $this->assertResponseOk();
    }

    public function testAllFilterBySupplier()
    {
        $response = $this->get('/ingredients?supplier=modern');
        $content = json_decode($response->response->content());
        $this->seeJsonStructure([
            'result',
            'page',
            'limit',
            'pages',
            'total'
        ]);
        $this->assertCount(3, $content->result);
        $this->assertResponseOk();
    }


    public function testDeleteWithValidId()
    {
        $this->delete('/ingredients/delete', ['id' => 1]);
        $this->seeJsonContains(['result' => true]);
        $this->assertResponseOk();
    }

    public function testDeleteWithNonExistingIdShouldReturnFalse()
    {
        $this->delete('/ingredients/delete', ['id' => 1000]);
        $this->seeJsonContains(['result' => false]);
        $this->assertResponseOk();
    }

    public function testDeleteWithoutIdShouldThrowAnError()
    {
        $this->delete('/ingredients/delete');
        $this->seeJsonContains([
            'message' => 'Ingredient id is required.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testDeleteWithNonIntegerIdShouldThrowAnError()
    {
        $this->delete('/ingredients/delete', ['id' => 'abc']);
        $this->seeJsonContains([
            'message' => 'Ingredient id must be an integer.'
        ]);
        $this->assertResponseStatus(500);
    }
}
