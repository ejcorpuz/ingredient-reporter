<?php

use Carbon\Carbon;

/**
 * Class UserBoxControllerTest
 */
class UserBoxControllerTest extends TestCase
{
    public function testCreateWithValidParameters()
    {
        $this->post('/boxes/create', [
            'delivery_date' => \Carbon\Carbon::now()->addDays(3)->format('Y-m-d'),
            'recipes' => [
                2, 3, 4
            ],
        ]);
        $this->assertResponseOk();
    }

    public function testCreateWithoutDeliveryDateShouldThrowAnError()
    {
        $this->post('/boxes/create', [
            'recipes' => [
                2, 3, 4
            ],
        ]);
        $this->seeJsonContains([
            'message' => 'Delivery date is required.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testCreateWithInvalidDeliveryDateFormatShouldThrowAnError()
    {
        $this->post('/boxes/create', [
            'delivery_date' => \Carbon\Carbon::now()->addDays(3)->format('m'),
            'recipes' => [
                2, 3, 4
            ],
        ]);
        $this->seeJsonContains([
            'message' => sprintf('Delivery date format should be YYYY-MM-DD, for example %s. Delivery date should be at least 48 hours from today.', Carbon::now()->format('Y-m-d'))
        ]);
        $this->assertResponseStatus(500);
    }

    public function testCreateWithDeliveryDateWithinFortyEightHoursShouldThrowAnError()
    {
        $this->post('/boxes/create', [
            'delivery_date' => \Carbon\Carbon::now()->format('Y-m-d'),
            'recipes' => [
                2, 3, 4
            ],
        ]);
        $this->seeJsonContains([
            'message' => 'Delivery date should be at least 48 hours from today.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testCreateWithoutRecipesShouldThrowAnError()
    {
        $this->post('/boxes/create', [
            'delivery_date' => \Carbon\Carbon::now()->addDays(3)->format('Y-m-d'),
        ]);
        $this->seeJsonContains([
            'message' => 'Recipes are required.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testCreateWithInvalidRecipeFormatShouldThrowAnError()
    {
        $this->post('/boxes/create', [
            'delivery_date' => \Carbon\Carbon::now()->addDays(3)->format('Y-m-d'),
            'recipes' => 1
        ]);
        $this->seeJsonContains([
            'message' => 'Recipes should be a collection or list format.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testCreateWithMoreThanFourRecipesShouldThrowAnError()
    {
        $this->post('/boxes/create', [
            'delivery_date' => \Carbon\Carbon::now()->addDays(3)->format('Y-m-d'),
            'recipes' => [2, 3, 4, 5, 6]
        ]);
        $this->seeJsonContains([
            'message' => 'Recipes should only have up to 4 items.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testCreateWithInvalidRecipeIdShouldThrowAnError()
    {
        $this->post('/boxes/create', [
            'delivery_date' => \Carbon\Carbon::now()->addDays(3)->format('Y-m-d'),
            'recipes' => [1000]
        ]);
        $this->seeJsonContains([
            'message' => 'Recipes should exists.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testAllWillReturnPaginatedItems()
    {
        $this->get('/boxes');
        $this->seeJsonStructure([
            'result',
            'page',
            'limit',
            'pages',
            'total'
        ]);
        $this->assertResponseOk();
    }

    public function testAllLimitToFiveItems()
    {
        $response = $this->get('/boxes?limit=5');
        $content = json_decode($response->response->content());
        $this->seeJsonStructure([
            'result',
            'page',
            'limit',
            'pages',
            'total'
        ]);
        $this->assertCount(5, $content->result);
        $this->assertResponseOk();
    }

    public function testAllFilterByDeliveryDate()
    {
        $response = $this->get('/boxes?delivery_date=' . Carbon::now()->format('Y-m-d'));
        $content = json_decode($response->response->content());
        $this->seeJsonStructure([
            'result',
            'page',
            'limit',
            'pages',
            'total'
        ]);
        $this->assertCount(1, $content->result);
        $this->assertResponseOk();
    }

    public function testDeleteWithValidId()
    {
        $this->delete('/boxes/delete', ['id' => 1]);
        $this->seeJsonContains(['result' => true]);
        $this->assertResponseOk();
    }

    public function testDeleteWithNonExistingIdShouldReturnFalse()
    {
        $this->delete('/boxes/delete', ['id' => 1000]);
        $this->seeJsonContains(['result' => false]);
        $this->assertResponseOk();
    }

    public function testDeleteWithoutIdShouldThrowAnError()
    {
        $this->delete('/boxes/delete');
        $this->seeJsonContains([
            'message' => 'Box id is required.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testDeleteWithNonIntegerIdShouldThrowAnError()
    {
        $this->delete('/boxes/delete', ['id' => 'abc']);
        $this->seeJsonContains([
            'message' => 'Box id must be an integer.'
        ]);
        $this->assertResponseStatus(500);
    }
}