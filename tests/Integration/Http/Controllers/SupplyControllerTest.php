<?php

use Carbon\Carbon;

/**
 * Class SupplyControllerTest
 */
class SupplyControllerTest extends TestCase
{
    public function testComputeWithValidParameters()
    {
        $response = $this->get('/supply/compute?order_date=' . \Carbon\Carbon::now()->format('Y-m-d'));
        $this->seeJsonStructure([
            [
                'id',
                'name',
                'measure',
                'amount',
            ]
        ]);
        $this->assertResponseOk();
    }

    public function testComputeWithoutOrderDateShouldThrowAnError()
    {
        $this->get('/supply/compute');
        $this->seeJsonContains([
            'message' => 'Order date is required.'
        ]);
        $this->assertResponseStatus(500);
    }

    public function testComputeWithInvalidOrderDateFormatShouldThrowAnError()
    {
        $this->get('/supply/compute?order_date=' . \Carbon\Carbon::now()->format('m'));
        $this->seeJsonContains([
            'message' => sprintf('Order date format should be YYYY-MM-DD, for example %s.', Carbon::now()->format('Y-m-d'))
        ]);
        $this->assertResponseStatus(500);
    }
}