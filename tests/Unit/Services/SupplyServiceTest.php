<?php

use App\Models\Ingredient;
use App\Models\Recipe;
use App\Models\RecipeIngredient;
use App\Models\UserBox;
use App\Models\UserBoxRecipe;
use App\Repositories\IngredientRepository;
use App\Repositories\UserBoxRepository;
use App\Services\SupplyService;
use Illuminate\Support\Collection;

/**
 * Class SupplyServiceTest
 */
class SupplyServiceTest extends TestCase
{
    /**
     * @var UserBoxRepository|\Mockery\Mock
     */
    protected $userBoxRepository;

    /**
     * @var IngredientRepository|\Mockery\Mock
     */
    protected $ingredientRepository;

    /**
     * @var SupplyService
     */
    protected $supplyService;

    /**
     * @var UserBox|\Mockery\Mock
     */
    protected $userBox;

    /**
     * @var UserBoxRecipe|\Mockery\Mock
     */
    protected $userBoxRecipe;

    /**
     * @var Recipe|\Mockery\Mock
     */
    protected $recipe;

    /**
     * @var RecipeIngredient|\Mockery\Mock
     */
    protected $recipeIngredient;

    public function setUp()
    {
        parent::setUp();

        $this->recipeIngredient = Mockery::mock(RecipeIngredient::class);
        $this->recipe = Mockery::mock(Recipe::class);
        $this->userBox = Mockery::mock(UserBox::class);
        $this->userBoxRecipe = Mockery::mock(UserBoxRecipe::class);
        $this->userBoxRepository = Mockery::mock(UserBoxRepository::class);
        $this->ingredientRepository = Mockery::mock(IngredientRepository::class);
        $this->supplyService = new SupplyService($this->userBoxRepository, $this->ingredientRepository);
    }

    public function testCompute()
    {
        $from = \Carbon\Carbon::now();
        $interval = 7;

        $this->ingredientRepository->shouldReceive('find')
            ->with(10)
            ->andReturn(new Ingredient());

        $this->recipeIngredient->shouldReceive('getAttribute')
            ->with('amount')
            ->andReturn(5);

        $this->recipeIngredient->shouldReceive('getAttribute')
            ->with('ingredient_id')
            ->andReturn(10);

        $this->recipe->shouldReceive('getAttribute')
            ->with('ingredients')
            ->andReturn(Collection::make([
                $this->recipeIngredient
            ]));

        $this->userBoxRecipe->shouldReceive('getAttribute')
            ->with('recipe')
            ->andReturn($this->recipe);

        $this->userBox->shouldReceive('getAttribute')
            ->with('recipes')
            ->andReturn(Collection::make([
                $this->userBoxRecipe
            ]));

        $this->userBoxRepository->shouldReceive('findByDeliveryDate')
            ->withAnyArgs()
            ->andReturn(Collection::make([
                $this->userBox
            ]));

        $result = $this->supplyService->compute($from, $interval);
        $this->assertInstanceOf(Collection::class, $result);
        $this->assertNotEmpty($result);
        $this->assertEquals(5, $result->first()->amount);
    }
}