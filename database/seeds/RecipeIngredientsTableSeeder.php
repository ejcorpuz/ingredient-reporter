<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RecipeIngredientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ingredients = [
            [
                'recipe_id' => 1,
                'ingredient_id' => 1,
                'amount' => 5
            ],
            [
                'recipe_id' => 1,
                'ingredient_id' => 2,
                'amount' => 5
            ],
            [
                'recipe_id' => 1,
                'ingredient_id' => 14,
                'amount' => 2
            ],
            [
                'recipe_id' => 1,
                'ingredient_id' => 5,
                'amount' => 2
            ],
            [
                'recipe_id' => 1,
                'ingredient_id' => 7,
                'amount' => 10
            ],
            //
            [
                'recipe_id' => 2,
                'ingredient_id' => 14,
                'amount' => 3
            ],
            [
                'recipe_id' => 2,
                'ingredient_id' => 4,
                'amount' => 8
            ],
            [
                'recipe_id' => 2,
                'ingredient_id' => 6,
                'amount' => 15
            ],
            [
                'recipe_id' => 2,
                'ingredient_id' => 1,
                'amount' => 5
            ],
            [
                'recipe_id' => 2,
                'ingredient_id' => 2,
                'amount' => 5
            ],
            //
            [
                'recipe_id' => 3,
                'ingredient_id' => 15,
                'amount' => 10
            ],
            [
                'recipe_id' => 3,
                'ingredient_id' => 16,
                'amount' => 10
            ],
            [
                'recipe_id' => 3,
                'ingredient_id' => 1,
                'amount' => 5
            ],
            [
                'recipe_id' => 3,
                'ingredient_id' => 2,
                'amount' => 5
            ],
            [
                'recipe_id' => 3,
                'ingredient_id' => 3,
                'amount' => 5
            ],
            [
                'recipe_id' => 3,
                'ingredient_id' => 4,
                'amount' => 5
            ],
            [
                'recipe_id' => 3,
                'ingredient_id' => 5,
                'amount' => 5
            ],
            [
                'recipe_id' => 3,
                'ingredient_id' => 9,
                'amount' => 5
            ],
            [
                'recipe_id' => 3,
                'ingredient_id' => 10,
                'amount' => 5
            ],
            [
                'recipe_id' => 3,
                'ingredient_id' => 11,
                'amount' => 5
            ],
            [
                'recipe_id' => 3,
                'ingredient_id' => 12,
                'amount' => 5
            ],
            //
            [
                'recipe_id' => 4,
                'ingredient_id' => 1,
                'amount' => 5
            ],
            [
                'recipe_id' => 4,
                'ingredient_id' => 2,
                'amount' => 5
            ],
            [
                'recipe_id' => 4,
                'ingredient_id' => 5,
                'amount' => 15
            ],
            [
                'recipe_id' => 4,
                'ingredient_id' => 6,
                'amount' => 15
            ],
            //
            [
                'recipe_id' => 5,
                'ingredient_id' => 7,
                'amount' => 20
            ],
            [
                'recipe_id' => 5,
                'ingredient_id' => 1,
                'amount' => 5
            ],
            [
                'recipe_id' => 5,
                'ingredient_id' => 2,
                'amount' => 5
            ],
            [
                'recipe_id' => 5,
                'ingredient_id' => 8,
                'amount' => 5
            ],
            //
            [
                'recipe_id' => 6,
                'ingredient_id' => 17,
                'amount' => 15
            ],
            [
                'recipe_id' => 6,
                'ingredient_id' => 8,
                'amount' => 5
            ],
            [
                'recipe_id' => 6,
                'ingredient_id' => 1,
                'amount' => 5
            ],
            [
                'recipe_id' => 6,
                'ingredient_id' => 2,
                'amount' => 5
            ],
            [
                'recipe_id' => 6,
                'ingredient_id' => 4,
                'amount' => 10
            ],
        ];

        foreach ($ingredients as $ingredient) {
            DB::table('recipe_ingredients')->insert($ingredient);
        }
    }
}
