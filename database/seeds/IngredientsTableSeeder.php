<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class IngredientsTableSeeder
 */
class IngredientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ingredients = [
            [
                'name' => 'salt',
                'measure' => 'g',
                'supplier' => 'modern'
            ],
            [
                'name' => 'pepper',
                'measure' => 'g',
                'supplier' => 'modern'
            ],
            [
                'name' => 'sugar',
                'measure' => 'g',
                'supplier' => 'modern'
            ],
            [
                'name' => 'potato',
                'measure' => 'pieces',
                'supplier' => 'fuji'
            ],
            [
                'name' => 'carrot',
                'measure' => 'pieces',
                'supplier' => 'fuji'
            ],
            [
                'name' => 'corn',
                'measure' => 'pieces',
                'supplier' => 'fuji'
            ],
            [
                'name' => 'onion',
                'measure' => 'pieces',
                'supplier' => 'fuji'
            ],
            [
                'name' => 'garlic',
                'measure' => 'pieces',
                'supplier' => 'fuji'
            ],
            [
                'name' => 'apple',
                'measure' => 'pieces',
                'supplier' => 'pak'
            ],
            [
                'name' => 'orange',
                'measure' => 'pieces',
                'supplier' => 'pak'
            ],
            [
                'name' => 'cabbage',
                'measure' => 'pieces',
                'supplier' => 'pak'
            ],
            [
                'name' => 'eggplant',
                'measure' => 'pieces',
                'supplier' => 'pak'
            ],
            [
                'name' => 'tomato',
                'measure' => 'pieces',
                'supplier' => 'pak'
            ],
            [
                'name' => 'beef',
                'measure' => 'kg',
                'supplier' => 'carrefour'
            ],
            [
                'name' => 'fish',
                'measure' => 'kg',
                'supplier' => 'carrefour'
            ],
            [
                'name' => 'chicken',
                'measure' => 'kg',
                'supplier' => 'carrefour'
            ],
            [
                'name' => 'mutton',
                'measure' => 'kg',
                'supplier' => 'carrefour'
            ],
        ];

        foreach ($ingredients as $ingredient) {
            DB::table('ingredients')->insert($ingredient);
        }
    }
}
