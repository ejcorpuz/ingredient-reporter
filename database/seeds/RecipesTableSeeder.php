<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RecipesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $recipes = [
            [
                'name' => 'Chicken Noodle Soup',
                'description' => 'Soup for the soul',
            ],
            [
                'name' => 'Beef Potato Corn',
                'description' => '',
            ],
            [
                'name' => 'Fish and Chicken Salad',
                'description' => 'Healthy Salad',
            ],
            [
                'name' => 'Corn and Carrots',
                'description' => '',
            ],
            [
                'name' => 'Onion Rings',
                'description' => '',
            ],
            [
                'name' => 'Grilled Garlic Mutton',
                'description' => 'Special Mutton Grilled',
            ],
        ];

        foreach ($recipes as $recipe) {
            DB::table('recipes')->insert($recipe);
        }
    }
}
