<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserBoxRecipesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $recipes = [
            ['user_box_id' => 1, 'recipe_id' => 1],
            ['user_box_id' => 1, 'recipe_id' => 5],
            ['user_box_id' => 1, 'recipe_id' => 3],

            ['user_box_id' => 2, 'recipe_id' => 6],
            ['user_box_id' => 2, 'recipe_id' => 2],

            ['user_box_id' => 3, 'recipe_id' => 4],

            ['user_box_id' => 4, 'recipe_id' => 2],

            ['user_box_id' => 5, 'recipe_id' => 5],
            ['user_box_id' => 5, 'recipe_id' => 4],
            ['user_box_id' => 5, 'recipe_id' => 3],
            ['user_box_id' => 5, 'recipe_id' => 2],

            ['user_box_id' => 6, 'recipe_id' => 6],
            ['user_box_id' => 6, 'recipe_id' => 3],
            ['user_box_id' => 6, 'recipe_id' => 2],
            ['user_box_id' => 6, 'recipe_id' => 1],

            ['user_box_id' => 7, 'recipe_id' => 2],
            ['user_box_id' => 7, 'recipe_id' => 3],

            ['user_box_id' => 8, 'recipe_id' => 6],

            ['user_box_id' => 9, 'recipe_id' => 3],
            ['user_box_id' => 9, 'recipe_id' => 4],
            ['user_box_id' => 9, 'recipe_id' => 5],

            ['user_box_id' => 10, 'recipe_id' => 2],
            ['user_box_id' => 10, 'recipe_id' => 3],

            ['user_box_id' => 11, 'recipe_id' => 6],
            ['user_box_id' => 11, 'recipe_id' => 5],

            ['user_box_id' => 12, 'recipe_id' => 1],

            ['user_box_id' => 13, 'recipe_id' => 6],
            ['user_box_id' => 13, 'recipe_id' => 5],
            ['user_box_id' => 13, 'recipe_id' => 4],
            ['user_box_id' => 13, 'recipe_id' => 2],

            ['user_box_id' => 14, 'recipe_id' => 3],
        ];

        foreach ($recipes as $recipe) {
            DB::table('user_box_recipes')->insert($recipe);
        }
    }
}
