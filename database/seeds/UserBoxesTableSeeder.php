<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserBoxesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $boxes = [
            ['delivery_date' => \Carbon\Carbon::now()],
            ['delivery_date' => \Carbon\Carbon::now()->addDays(1)],
            ['delivery_date' => \Carbon\Carbon::now()->addDays(2)],
            ['delivery_date' => \Carbon\Carbon::now()->addDays(2)],
            ['delivery_date' => \Carbon\Carbon::now()->addDays(3)],
            ['delivery_date' => \Carbon\Carbon::now()->addDays(3)],
            ['delivery_date' => \Carbon\Carbon::now()->addDays(3)],
            ['delivery_date' => \Carbon\Carbon::now()->addDays(4)],
            ['delivery_date' => \Carbon\Carbon::now()->addDays(5)],
            ['delivery_date' => \Carbon\Carbon::now()->addDays(5)],
            ['delivery_date' => \Carbon\Carbon::now()->addDays(5)],
            ['delivery_date' => \Carbon\Carbon::now()->addDays(6)],
            ['delivery_date' => \Carbon\Carbon::now()->addDays(6)],
            ['delivery_date' => \Carbon\Carbon::now()->addDays(6)],
        ];

        foreach ($boxes as $box) {
            DB::table('user_boxes')->insert($box);
        }
    }
}
