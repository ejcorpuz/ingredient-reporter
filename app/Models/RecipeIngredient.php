<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class RecipeIngredient
 * @package App\Models
 */
class RecipeIngredient extends Model
{
    /**
     * @var string
     */
    protected $table = 'recipe_ingredients';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $with = ['ingredient'];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * @return HasOne
     */
    public function recipe()
    {
        return $this->hasOne(Recipe::class, 'id', 'recipe_id');
    }

    /**
     * @return HasOne
     */
    public function ingredient()
    {
        return $this->hasOne(Ingredient::class, 'id', 'ingredient_id');
    }
}