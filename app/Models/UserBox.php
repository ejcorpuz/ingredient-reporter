<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserBox
 * @package App\Models
 */
class UserBox extends Model
{
    /**
     * @var string
     */
    protected $table = 'user_boxes';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $with = ['recipes'];

    /**
     * @var array
     */
    protected $casts = [
        'delivery_date' => 'date'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recipes()
    {
        return $this->hasMany(UserBoxRecipe::class, 'user_box_id', 'id');
    }
}