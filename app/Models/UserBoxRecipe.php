<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserBoxRecipe
 * @package App\Models
 */
class UserBoxRecipe extends Model
{
    /**
     * @var string
     */
    protected $table = 'user_box_recipes';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $with = ['recipe'];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function recipe()
    {
        return $this->hasOne(Recipe::class, 'id', 'recipe_id');
    }
}