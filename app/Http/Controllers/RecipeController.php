<?php

namespace App\Http\Controllers;

use App\Repositories\RecipeRepository;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class RecipeController
 * @package App\Http\Controllers
 */
class RecipeController extends Controller
{
    /**
     * @var RecipeRepository
     */
    protected $recipeRepository;

    /**
     * RecipeController constructor.
     * @param RecipeRepository $recipeRepository
     */
    public function __construct(RecipeRepository $recipeRepository)
    {
        $this->recipeRepository = $recipeRepository;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function all(Request $request)
    {
        $this->validate($request, [
            'page' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ], []);

        $page = (int)$request->get('page', 1);
        $limit = (int)$request->get('limit', 10);

        $result = $this->recipeRepository->findAll($page, $limit);
        $count = $this->recipeRepository->countAll();

        return response()->json([
            'result' => $result->toArray(),
            'page' => $page,
            'limit' => $limit,
            'pages' => ceil($count / $limit),
            'total' => $count,
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function create(Request $request)
    {
        $this->validateCreateRecipe($request);

        $name = (string)$request->get('name');
        $description = (string)$request->get('description');
        $ingredients = (array)$request->get('ingredients');

        $attributes = [
            'name' => $name,
            'description' => $description,
        ];

        $recipe = $this->recipeRepository->createWithIngredients($attributes, $ingredients);
        $result = $this->recipeRepository->find($recipe->id);

        return response()->json($result->toArray());
    }

    /**
     * @param Request $request
     * @throws ValidationException
     */
    protected function validateCreateRecipe(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:recipes,name',
            'description' => 'nullable|string',
            'ingredients' => 'required|array',
            'ingredients.*.ingredient_id' => 'required|exists:ingredients,id',
            'ingredients.*.amount' => 'required|numeric'
        ], [
            'name.required' => 'Recipe name is required.',
            'name.string' => 'Recipe name should be a valid string.',
            'name.unique' => 'Recipe with the same name already exist.',
            'ingredients.required' => 'Ingredients is required to create a recipe.',
            'ingredients.array' => 'Ingredients must be a list or collection.',
            'ingredients.*.ingredient_id.required' => 'A valid ingredient is required.',
            'ingredients.*.ingredient_id.exists' => 'A valid ingredient is required.',
            'ingredients.*.amount.required' => 'Ingredient amount is required.',
            'ingredients.*.amount.numeric' => 'Ingredient amount should be a valid value.',
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws Exception
     */
    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer'
        ], [
            'id.required' => 'Recipe id is required.',
            'id.integer' => 'Recipe id must be an integer.'
        ]);

        $id = (int)$request->get('id');

        $result = $this->recipeRepository->delete($id);

        return response()->json(['result' => $result]);
    }
}