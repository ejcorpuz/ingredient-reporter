<?php

namespace App\Http\Controllers;

use App\Services\SupplyService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class SupplyController
 * @package App\Http\Controllers
 */
class SupplyController extends Controller
{
    /**
     * @var SupplyService
     */
    protected $supplyService;

    /**
     * SupplyController constructor.
     * @param SupplyService $supplyService
     */
    public function __construct(SupplyService $supplyService)
    {
        $this->supplyService = $supplyService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function compute(Request $request)
    {
        $this->validate($request, [
            'order_date' => 'required|date_format:Y-m-d'
        ], [
            'order_date.required' => 'Order date is required.',
            'order_date.date_format' => sprintf('Order date format should be YYYY-MM-DD, for example %s.', Carbon::now()->format('Y-m-d')),
        ]);

        $orderDate = Carbon::createFromFormat('Y-m-d', (string)$request->get('order_date'));

        $supply = $this->supplyService->compute($orderDate, 7);

        return response()->json($supply->toArray());
    }
}