<?php

namespace App\Http\Controllers;

use App\Repositories\UserBoxRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class UserBoxController
 * @package App\Http\Controllers
 */
class UserBoxController extends Controller
{
    /**
     * @var UserBoxRepository
     */
    protected $userBoxRepository;

    /**
     * UserBoxController constructor.
     * @param UserBoxRepository $userBoxRepository
     */
    public function __construct(UserBoxRepository $userBoxRepository)
    {
        $this->userBoxRepository = $userBoxRepository;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function all(Request $request)
    {
        $this->validate($request, [
            'page' => 'nullable|integer',
            'limit' => 'nullable|integer',
            'delivery_date' => 'nullable|date_format:Y-m-d'
        ], []);

        $page = (int)$request->get('page', 1);
        $limit = (int)$request->get('limit', 10);
        $deliveryDate = (string)$request->get('delivery_date');

        $filter = ['delivery_date' => $deliveryDate];
        $result = $this->userBoxRepository->findAllFiltered($page, $limit, $filter);
        $count = $this->userBoxRepository->countAllFiltered($filter);

        return response()->json([
            'result' => $result->toArray(),
            'page' => $page,
            'limit' => $limit,
            'pages' => ceil($count / $limit),
            'total' => $count,
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'delivery_date' => 'required|date_format:Y-m-d|after:' . Carbon::now()->addDays(2)->format('Y-m-d'),
            'recipes' => 'required|array|max:4|exists:recipes,id',
        ], [
            'delivery_date.required' => 'Delivery date is required.',
            'delivery_date.date_format' => sprintf('Delivery date format should be YYYY-MM-DD, for example %s.', Carbon::now()->format('Y-m-d')),
            'delivery_date.after' => 'Delivery date should be at least 48 hours from today.',
            'recipes.required' => 'Recipes are required.',
            'recipes.array' => 'Recipes should be a collection or list format.',
            'recipes.max' => 'Recipes should only have up to 4 items.',
            'recipes.exists' => 'Recipes should exists.',
        ]);

        $deliveryDate = Carbon::createFromFormat('Y-m-d', (string)$request->get('delivery_date'));
        $recipes = (array)$request->get('recipes');

        $attributes = [
            'delivery_date' => $deliveryDate
        ];

        $box = $this->userBoxRepository->createWithRecipes($attributes, $recipes);
        $result = $this->userBoxRepository->find($box->id);

        return response()->json($result->toArray());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws Exception
     */
    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer'
        ], [
            'id.required' => 'Box id is required.',
            'id.integer' => 'Box id must be an integer.'
        ]);

        $id = (int)$request->get('id');

        $result = $this->userBoxRepository->delete($id);

        return response()->json(['result' => $result]);
    }
}