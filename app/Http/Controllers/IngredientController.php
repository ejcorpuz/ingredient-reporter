<?php

namespace App\Http\Controllers;

use App\Repositories\IngredientRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Exception;

/**
 * Class IngredientController
 * @package App\Http\Controllers
 */
class IngredientController extends Controller
{
    /**
     * @var IngredientRepository
     */
    protected $ingredientRepository;

    /**
     * IngredientController constructor.
     * @param IngredientRepository $ingredientRepository
     */
    public function __construct(IngredientRepository $ingredientRepository)
    {
        $this->ingredientRepository = $ingredientRepository;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function all(Request $request)
    {
        $this->validate($request, [
            'page' => 'nullable|integer',
            'limit' => 'nullable|integer',
            'supplier' => 'nullable|string'
        ], []);

        $page = (int)$request->get('page', 1);
        $limit = (int)$request->get('limit', 10);
        $supplier = (string)$request->get('supplier', null);

        $filter = ['supplier' => $supplier];
        $result = $this->ingredientRepository->findAllFiltered($page, $limit, $filter);
        $count = $this->ingredientRepository->countAllFiltered($filter);

        return response()->json([
            'result' => $result->toArray(),
            'page' => $page,
            'limit' => $limit,
            'pages' => ceil($count / $limit),
            'total' => $count,
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:ingredients,name',
            'measure' => 'required|in:g,kg,pieces',
            'supplier' => 'required|string',
        ], [
            'name.required' => 'Ingredient name is required.',
            'name.string' => 'Ingredient name should be a valid string.',
            'name.unique' => 'Ingredient with the same name already exist.',
            'measure.required' => 'Ingredient measure is required, either g, kg, pieces.',
            'measure.in' => 'Ingredient measure should be either g, kg, pieces.',
            'supplier.required' => 'Ingredient supplier name is required.',
            'supplier.string' => 'Ingredient supplier name should be a valid string.',
        ]);

        $name = (string)$request->get('name');
        $measure = (string)$request->get('measure');
        $supplier = (string)$request->get('supplier');

        $result = $this->ingredientRepository->create([
            'name' => $name,
            'measure' => $measure,
            'supplier' => $supplier
        ]);

        return response()->json($result->toArray());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws Exception
     */
    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer'
        ], [
            'id.required' => 'Ingredient id is required.',
            'id.integer' => 'Ingredient id must be an integer.'
        ]);

        $id = (int)$request->get('id');

        $result = $this->ingredientRepository->delete($id);

        return response()->json(['result' => $result]);
    }
}