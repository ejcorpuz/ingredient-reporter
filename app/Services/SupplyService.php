<?php

namespace App\Services;

use App\Models\RecipeIngredient;
use App\Models\UserBox;
use App\Models\UserBoxRecipe;
use App\Repositories\IngredientRepository;
use App\Repositories\UserBoxRepository;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Collection;

/**
 * Class SupplyService
 * @package App\Services
 */
class SupplyService
{
    /**
     * @var UserBoxRepository
     */
    protected $userBoxRepository;

    /**
     * @var IngredientRepository
     */
    protected $ingredientRepository;

    /**
     * SupplyService constructor.
     * @param UserBoxRepository $userBoxRepository
     * @param IngredientRepository $ingredientRepository
     */
    public function __construct(UserBoxRepository $userBoxRepository, IngredientRepository $ingredientRepository)
    {
        $this->userBoxRepository = $userBoxRepository;
        $this->ingredientRepository = $ingredientRepository;
    }

    /**
     * @param Carbon $from
     * @param int $interval
     * @return Collection
     */
    public function compute(Carbon $from, int $interval = 7)
    {
        $period = CarbonPeriod::create(
            $from->copy()->startOfDay(),
            $from->copy()->addDays($interval - 1)->endOfDay()
        );

        $orders = $this->userBoxRepository->findByDeliveryDate($period->first(), $period->last());

        $ingredients = new Collection();
        $orders->each(function (UserBox $order) use (&$ingredients) {
            $order->recipes->each(function (UserBoxRecipe $boxRecipe) use (&$ingredients) {
                $boxRecipe->recipe->ingredients->each(function (RecipeIngredient $recipeIngredient) use (&$ingredients) {
                    $amount = $ingredients->get($recipeIngredient->ingredient_id, 0) + $recipeIngredient->amount;
                    $ingredients->put($recipeIngredient->ingredient_id, $amount);
                });
            });
        });

        return $ingredients->map(function ($value, $key) {
            $ingredient = $this->ingredientRepository->find($key);
            $ingredient->amount = $value;
            return $ingredient;
        })->values();
    }
}