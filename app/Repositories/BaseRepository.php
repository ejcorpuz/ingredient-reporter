<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Exception;

/**
 * Class BaseRepository
 * @package App\Repositories
 */
abstract class BaseRepository
{
    /**
     * @var Builder
     */
    protected $model;

    /**
     * @param int $page
     * @param int $limit
     * @return Collection
     */
    public function findAll(int $page = 1, int $limit = 10)
    {
        $offset = abs(($page - 1) * $limit);
        return $this->model->offset($offset)->limit($limit)->get();
    }

    /**
     * @param int $page
     * @param int $limit
     * @param array $filters
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function findAllFiltered(int $page = 1, int $limit = 10, array $filters = [])
    {
        $offset = abs(($page - 1) * $limit);
        $model = $this->model;
        foreach ($filters as $field => $value) {
            $model = $model->orWhere($field, 'like', '%' . $value . '%');
        }
        return $model->offset($offset)->limit($limit)->get();
    }

    /**
     * @param array $filters
     * @return int
     */
    public function countAllFiltered(array $filters = [])
    {
        $model = $this->model;
        foreach ($filters as $field => $value) {
            $model = $model->orWhere($field, 'like', '%' . $value . '%');
        }
        return $model->count();
    }

    /**
     * @return int
     */
    public function countAll()
    {
        return $this->model->count();
    }

    /**
     * @param array $attributes
     * @return Model
     */
    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    /**
     * @param int $id
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function find(int $id)
    {
        return $this->model->find($id);
    }

    /**
     * @param int $id
     * @return bool|mixed|null
     * @throws Exception
     */
    public function delete(int $id)
    {
        $data = $this->model->find($id);
        if ($data) {
            return $data->delete();
        }

        return false;
    }
}