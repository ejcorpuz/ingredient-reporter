<?php

namespace App\Repositories;

use App\Models\Recipe;

/**
 * Class RecipeRepository
 * @package App\Repositories
 */
class RecipeRepository extends BaseRepository
{
    /**
     * RecipeRepository constructor.
     * @param Recipe $model
     */
    public function __construct(Recipe $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $attributes
     * @param array $ingredients
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createWithIngredients(array $attributes, array $ingredients)
    {
        $recipe = $this->create($attributes);
        $recipe->ingredients()->createMany($ingredients);
        $recipe->refresh();

        return $recipe;
    }
}