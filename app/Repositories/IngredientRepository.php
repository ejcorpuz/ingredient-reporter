<?php

namespace App\Repositories;

use App\Models\Ingredient;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class IngredientRepository
 * @package App\Repositories
 */
class IngredientRepository extends BaseRepository
{
    /**
     * IngredientRepository constructor.
     * @param Ingredient|Builder $model
     */
    public function __construct(Ingredient $model)
    {
        $this->model = $model;
    }
}