<?php

namespace App\Repositories;

use App\Models\UserBox;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;

/**
 * Class UserBoxRepository
 * @package App\Repositories
 */
class UserBoxRepository extends BaseRepository
{
    /**
     * UserBoxRepository constructor.
     * @param UserBox|Builder $model
     */
    public function __construct(UserBox $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $attributes
     * @param array $recipes
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createWithRecipes(array $attributes, array $recipes)
    {
        $recipes = array_map(function ($id) {
            return ['recipe_id' => $id];
        }, $recipes);

        $box = $this->create($attributes);
        $box->recipes()->createMany($recipes);
        $box->refresh();

        return $box;
    }

    /**
     * @param Carbon $from
     * @param Carbon $to
     * @return \Illuminate\Support\Collection
     */
    public function findByDeliveryDate(Carbon $from, Carbon $to)
    {
        return $this->model->whereBetween('delivery_date', [$from, $to])->get();
    }
}